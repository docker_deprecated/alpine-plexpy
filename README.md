# alpine-plexpy

#### [alpine-x64-plexpy](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-plexpy/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64build/alpine-x64-plexpy.svg)](https://microbadger.com/images/forumi0721alpinex64build/alpine-x64-plexpy "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64build/alpine-x64-plexpy.svg)](https://microbadger.com/images/forumi0721alpinex64build/alpine-x64-plexpy "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64build/alpine-x64-plexpy.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-plexpy/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64build/alpine-x64-plexpy.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-plexpy/)
#### [alpine-aarch64-plexpy](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-plexpy/)
[![](https://images.microbadger.com/badges/version/forumi0721alpineaarch64build/alpine-aarch64-plexpy.svg)](https://microbadger.com/images/forumi0721alpineaarch64build/alpine-aarch64-plexpy "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpineaarch64build/alpine-aarch64-plexpy.svg)](https://microbadger.com/images/forumi0721alpineaarch64build/alpine-aarch64-plexpy "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64build/alpine-aarch64-plexpy.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-plexpy/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64build/alpine-aarch64-plexpy.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-plexpy/)
#### [alpine-armhf-plexpy](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-plexpy/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinearmhf/alpine-armhf-plexpy.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-plexpy "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinearmhf/alpine-armhf-plexpy.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-plexpy "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-plexpy.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-plexpy/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-plexpy.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-plexpy/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : PlexPy
    - A python based web application for monitoring, analytics and notifications for Plex Media Server.
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
    - [forumi0721/alpine-aarch64-base](https://hub.docker.com/r/forumi0721/alpine-aarch64-base/)
    - [forumi0721/alpine-armhf-base](https://hub.docker.com/r/forumi0721/alpine-armhf-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -d \
           -p 8181:8181/tcp \
           forumi0721alpinex64build/alpine-x64-plexpy:latest
```

* aarch64
```sh
docker run -d \
           -p 8181:8181/tcp \
           forumi0721alpineaarch64build/alpine-aarch64-plexpy:latest
```

* armhf
```sh
docker run -d \
           -p 8181:8181/tcp \
           forumi0721alpinearmhfbuild/alpine-armhf-plexpy:latest
```



----------------------------------------
#### Usage

* URL : [http://localhost:8181/](http://localhost:8181/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 8181/tcp           | Serivce port                                     |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |



----------------------------------------
* [forumi0721alpinex64build/alpine-x64-plexpy](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-plexpy/)
* [forumi0721alpineaarch64build/alpine-aarch64-plexpy](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-plexpy/)
* [forumi0721alpinearmhfbuild/alpine-armhf-plexpy](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-plexpy/)

